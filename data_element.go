// :vi ts=4 sw=4 noet:

/*
	Mnemonic:	data_element.go
	Abstract:	Manages a single element in the data.  Each element has two values: sequence and 
				value where sequence can be something like a timestamp, or just a sequential value.
				For data that is coordinate oriented (x.y)  sequence is the X value and value is the 
				Y. To make code easier to read both functions using seq,val and x,y pairs exist.
	Date:		12 May 2018
	Author:		Scott Daniels
*/

package data_mgr

import (
	"fmt"
)

// ----- data elment things---------------------------------------------------

/*
	Manages one sequence,value pair.
*/
type Data_element struct {
	sequence	float64		// could be timestamp, could be record number or could be a true x for scatter plot
	value		float64		// 'height' for bar/line graphs, y for scatter
}


/*
	Mk_data_ele crates a data element with the seq,val (x,y) pair.
	Pointer to the element is returned.
*/
func Mk_data_ele( seq float64, value float64 ) ( *Data_element ) {
	de := &Data_element{
		sequence:	seq,
		value:		value,
	}

	return de
}

/*
	Get_sv returns the sequence an value information from the element.
*/
func ( de *Data_element ) Get_sv( ) ( seq float64, val float64 ) {
	if de == nil {
		return 0.0, 0.0
	}

	return de.sequence, de.value
}

/*
	Get_xy returns the "x,y"  pair.
*/
func ( de *Data_element ) Get_xy( ) ( seq float64, val float64 ) {
	if de == nil {
		return 0.0, 0.0
	}

	return de.sequence, de.value
}

/*
	String generates an ordered pair in string format.
*/
func ( de *Data_element ) String( ) ( string ) {
	if de == nil {
		return "(0.0,0.0)"
	}

	return fmt.Sprintf(	"(%.3f,%.3f)", de.sequence, de.value )
}
