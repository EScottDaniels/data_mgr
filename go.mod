module bitbucket.org/EScottDaniels/data_mgr

go 1.15

require (
	gitlab.com/rouxware/goutils/clike v1.0.0
	gitlab.com/rouxware/goutils/token v1.0.0
)
