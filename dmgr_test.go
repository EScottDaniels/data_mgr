
package data_mgr

import (
	"fmt"
	"os"
	"testing"
)

func TestData_groups( t *testing.T ) {
	fmt.Fprintf( os.Stderr, "hello world\n" )

	seq_col := -1
	sets := 3
	set_size := 7
	dg := Mk_data_group( sets, set_size, nil )
	err := dg.Load_comms_sets( "test.data", " ", seq_col, "1 2 4" )
	if err == nil {
		fmt.Fprintf( os.Stderr, ">>> %s\n", dg )
	} else {
		fmt.Fprintf( os.Stderr, "load failed: %s\n", err )
		t.Fail()
		return
	}

	dg2 := Mk_data_group( 1, set_size, nil )
	err = dg2.Load_comms_sets( "test2.data", " ", 0, "1" )
	if err != nil {
		fmt.Fprintf( os.Stderr, ">>> error loading data 2\n" )
		t.Fail()
		return
	}

	dg.Add_group( dg2 )			// combine the data
	fmt.Fprintf( os.Stderr, "combined data >>> %s\n", dg )

	mn, mx := dg.Get_seq_range()
	fmt.Fprintf( os.Stderr, "seq range: %.2f - %.2f\n", mn, mx )

	mn, mx = dg.Get_value_range()
	fmt.Fprintf( os.Stderr, "val range: %.2f - %.2f\n", mn, mx )

	nval := dg.Get_nvalues( 0,  0, 100 ) 
	for i, v := range nval {
		fmt.Printf( "[%d] %03f\n", i, v )
	}
}

func TestData_set( t *testing.T ) {
	f, err := os.Create( "./foo.data"  ) 
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] cannot open file to write test data into: %s\n", err )
		t.Fail()
		return
	}

	fmt.Fprintf( os.Stderr, "\n[INFO] tests with internally crated data begins\n" )
	d1 := 0
	d2 := float64( 0.0 )
	d3 := 0

	for i := 0; i < 1000; i++ {
		fmt.Fprintf( f, "%d %d %.3f %d\n", i, d1, d2, d3 )
		d1 += 10;
		d2 += .1;
		d3 += 100
	}

	f.Close()

	sets := 1
	set_size := 75
	dg := Mk_data_group( sets, set_size, nil )
	err = dg.Load_comms_sets( "foo.data", " ", 0, "2" )
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] cannot load from test data foo.data %s\n", err )
		t.Fail()
		return
	}
	
	nval := dg.Get_nvalues( 0, 0, 1000 ) 
	if len( nval ) != 75 {
		fmt.Fprintf( os.Stderr, "[FAIL] expected 75 data elements, got %d\n", len( nval ) )
		t.Fail()
		return
	}

	errors := 0
	for i, v := range nval {
		if v == 0 {
			fmt.Fprintf( os.Stderr, "[FAIL] unexpected value of 0 at index %d\n", i )
			errors++
		}
	}

	if errors > 0 {
		t.Fail()
		return
	}

	
	fmt.Fprintf( os.Stderr, "[OK]   all values wer >0 as expected\n" )
}
