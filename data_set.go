// :vi ts=4 sw=4 noet:

/*
	Mnemonic:	data_set.go
	Abstract:	A set of data values for plotting. Data sets are maintained with original values and
				normlised for plotting only wheen needed.  Repeated calls to normalise return the same
				value set as long as no values, or min/max, have changed.
	Date:		12 May 2018
	Author:		Scott Daniels
*/

package data_mgr

import (
	"fmt"
)

type Data_set struct {
	colour		string			// colour assigned to the set
	elements	[]*Data_element	// the actual data

	max_val		float64			// min/max observed value in the set
	min_val		float64
	max_seq		float64			// min/max sequence obsrved
	min_seq		float64
}

/*
	Mk_data_set creates a set with at most size values. Assigns a colour to the set. Min and max
	are the intial value range.
*/
func Mk_data_set( size int, colour string ) ( *Data_set ) {
	ds := &Data_set{
		colour: colour,
		elements: make( []*Data_element, 0, size ),
		min_val:	0,
		max_val:	0,
		min_seq:	0,
		max_seq:	0,	
	}

	return ds
}

/*
	Add_sv adds a sequence/value pair to the data set.
*/
func ( ds *Data_set ) Add_sv( seq float64, value float64 ) {
	if ds == nil {
		return 
	}

	reset_minmax := false				// if we need to force a complete rescan for min/max, this will flip on

	if len( ds.elements ) == cap( ds.elements ) {		// full, pop off the head (0) and schedule min/max recan if needed
		olds, oldv := ds.elements[0].Get_sv() 
		if olds == ds.min_seq || olds == ds.max_seq  ||  oldv == ds.min_val || oldv == ds.max_val  {		// recan if a deleted value was a min/max
			reset_minmax = true
		}

		tmp := make( []*Data_element, len( ds.elements ) -1, len( ds.elements )  )	// create a new space, then copy from [1] to end to it to 'pop' the 0th element
		copy( tmp, ds.elements[1:] )
		ds.elements = tmp
	}

	ds.elements = append( ds.elements, Mk_data_ele( seq, value ) )		// add to tail

	start := len( ds.elements ) - 1							// for most start for minmax check is newly added element
	if reset_minmax {									// unless rescan all is needed where we start at 0
		start = 0
	}

	for i := start; i < len( ds.elements ); i++ {
		seq, val := ds.elements[i].Get_sv() 
		if i == 0 {
			ds.max_seq = seq
			ds.min_seq = seq
			ds.max_val = val
			ds.min_val = val
		} else {
			if seq < ds.min_seq  {
				ds.min_seq = seq
			} else  { 
				if seq > ds.max_seq  {
					ds.max_seq = seq
				}
			}
		
			if val < ds.min_val   {
				ds.min_val = val
			}  else {
				if val > ds.max_val  {
					ds.max_val = val
				}
			}
		}
	}
}


/*
	Clear completely clears the data from the set.
*/
func ( ds *Data_set ) Clear( ) ( ) {
	if ds == nil {
		return
	}

	ds.elements = make( []*Data_element, 0, cap( ds.elements ) )
}

/*
	Get_colour returns the current colour associated with the set.
*/
func ( ds *Data_set ) Get_colour( ) ( string ) {
	if ds == nil {
		return "white"
	}

	return ds.colour
}

/*
	Get_nvalues returns a slice of normalised values over the range of 0 to 1. Forced min/max are
	used to ensure a minimum range. For example, if the normal range of data is 20-80, but the 
	desired set should be normalised over 0-100, then the forced_min/max passed in should be
	0 and 100 respectively.  If forced_min/max are the same (e.g. 0,0) then the 'natural' range
	of the data is used. 
*/
func ( ds *Data_set ) Get_nvalues( forced_min float64, forced_max float64 ) ( []float64 ) {
	if ds == nil {
		return nil
	}

	if forced_min == forced_max {			// just use what we have
		forced_min = ds.min_val
		forced_max = ds.max_val
	} else {
		if ds.min_val < forced_min {
			forced_min = ds.min_val
		}
		if ds.max_val > forced_max {
			forced_max = ds.max_val
		}
	}

	nconst := forced_max - forced_min		// normalisation constant
	if nconst == 0 {
		nconst = 1
	}

	eles := ds.elements					// if elements is updated in another thread it could be reallocaed; grab a reference to prevent issues
	l := len( eles )					// ds could be extended asynch, so work with a constant length
	rs := make( []float64, l )
	for i := 0; i < l; i++ {
		_, v := eles[i].Get_sv() 
		rs[i] = v / nconst
	}

	return rs
}

/*
	Get_nseq returns a slice of normalised sequences over the range of 0 to 1. Forced min/max are
	used to ensure a minimum range. For example, if the normal range of data is 20-80, but the 
	desired set should be normalised over 0-100, then the forced_min/max passed in should be
	0 and 100 respectively.  If forced_min/max are the same (e.g. 0,0) then the 'natural' range
	of the data is used. 
*/
func ( ds *Data_set ) Get_nseq( forced_min float64, forced_max float64 ) ( []float64 ) {
	if ds == nil {
		return nil
	}

	if forced_min == forced_max {			// just use what we have
		forced_min = ds.min_seq
		forced_max = ds.max_seq
	} else {
		if ds.min_seq < forced_min {
			forced_min = ds.min_seq
		}
		if ds.max_seq > forced_max {
			forced_max = ds.max_seq
		}
	}

	nconst := forced_max - forced_min		// normalisation constant
	if nconst == 0 {
		nconst = 1
	}

	rs := make( []float64, len( ds.elements ) )
	for i := range ds.elements {
		s, _ := ds.elements[i].Get_sv() 
		rs[i] = s / nconst
	}

	return rs
}

/*
	Get_rvalues returns a slice of the values that are unadjusted.
*/
func ( ds *Data_set ) Get_rvalues(  ) ( []float64 ) {
	if ds == nil {
		return nil
	}

	rs := make( []float64, len( ds.elements ) )		// create the return slice
	for i := range ds.elements {
		_, rs[i] = ds.elements[i].Get_sv() 
	}

	return rs
}

/*
	Get_nelements returns a slice of elements which have been normalised over the seq/val (x,y) 
	min max range. Forced_s_min/forced_s-max and/or forced_v_min/forced_v_max are used to provide
	a desired range for each value type.  If forced values are the same (e.g. -1,-1) then the 
	natural occuring range for the tyupe (seq or value) is used.
*/
func ( ds *Data_set ) Get_nelements( 
		forced_s_min float64,  
		forced_s_max float64,
		forced_v_min float64,
		forced_v_max float64, ) ( []*Data_element ) {

	if ds == nil {
		return nil
	}

	n_seq := ds.Get_nseq( forced_s_min, forced_s_max )
	n_val := ds.Get_nvalues( forced_s_min, forced_s_max )

	if len( n_seq ) != len( n_val ) {			// trust but verify
		return nil
	}
	
	re := make( []*Data_element, len( n_val ) )
	for i, v := range n_val {
		re[i] = Mk_data_ele( n_seq[i], v )
	}

	return re
}


/*
	Get_seq_range returns  the swquence range (min/max).
*/
func ( ds *Data_set ) Get_seq_range( ) ( min float64, max float64 ) {
	if ds == nil {
		return 0.0, 0.0
	}

	return ds.min_seq, ds.max_seq
}

/*
	Size returns the number of values registered in the set.
*/
func ( ds *Data_set ) Size( ) ( int ) {
	if ds == nil {
		return 0
	}

	return len( ds.elements )
}

/*
	Get_value_range returns the value range (min/max).
*/
func ( ds *Data_set ) Get_value_range( ) ( min float64, max float64 ) {
	if ds == nil {
		return 0.0, 0.0
	}

	return ds.min_val, ds.max_val
}

// ----------------- mostly debugging ----------------------------------
  
/*
	String implements the Go stringer interface.
*/
func ( ds *Data_set ) String( ) ( string ) {
	if ds == nil {
		return "<nil>"
	}

	s := "data_set: [ "
	for _, e := range ds.elements {
		s += fmt.Sprintf( "\n%s", e )
	}

	s += "\n ]"

	return s
}

