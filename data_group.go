// :vi ts=4 sw=4 noet:

/*
	Mnemonic:	data_group.go
	Abstract:	Manages a group of ddatasets for things like multi-line graphs.
	Date:		15 May 2018
	Author:		Scott Daniels
*/

package data_mgr

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"gitlab.com/rouxware/goutils/clike"
	"gitlab.com/rouxware/goutils/token"
)

type Data_group struct {
	sets []*Data_set
	active *Data_set			// the active set for unreferenced add etc.
	set_size	int				// number of elements in each set
}

const (
	ACTIVE	int = -1			// pass in place of the set number to operate on the active set
)

var (
	ERR_NIL_GROUP	error = fmt.Errorf( "nil group value" )
	ERR_RANGE		error = fmt.Errorf( "parameter/value out of range" )
)

/*
	Mk_data_group creates a group structure with the specified number of underlying
	data sets (num_sets) each of the indicated set size. Colours may be a slice of strings,
	or a stirng of comma separated colour names/values. If nil, then a default list of colours
	is used:
			red, green, blue, oragne, yellow, grey, pink and aqua.

	The first set in the group is selected as the active set.
*/
func Mk_data_group( num_sets int, set_size int, icolours interface{} ) ( dg *Data_group ) {
	var( 
		colours []string
	)

	dg = &Data_group {
		sets: make( []*Data_set, num_sets ),
		set_size: set_size,
	}	

	switch cval := icolours.(type) {
		case []string:
			colours = cval

		case string:
			colours = strings.Split( cval, "," )

		case *string:
			colours = strings.Split( *cval, "," )
	}

	if colours == nil {
		colours = []string{ "red", "green", "blue", "orange", "yellow", "grey", "pink", "aqua" }
	}

	c := 0
	for i := 0; i < num_sets; i++ {
		dg.sets[i] = Mk_data_set( set_size, colours[c] )
		c += 1
		if c >= len( colours ) {
			c = 0
		}
	}

	dg.active = dg.sets[0]
	return dg
}

/*
	Select the set which will be the target of all 'generic' operations.
*/
func ( dg *Data_group ) Select( which int ) ( error ) {
	if dg == nil {
		return ERR_NIL_GROUP
	}

	if which < 0 || which > len( dg.sets ) {
		return ERR_RANGE
	}

	dg.active = dg.sets[which]

	return nil
}

/*
	Get a normalised set of values from the nth set in the group.
	If forced_min and forced_max are different values, then they will be used
	to force the range indicated.  If the data naturally exceedes the range,
	in one or both directions, then that range will be used. E.g using a forced 
	min/max of 0, 100 ensures that the data is normalised over the range 0..100.
	If the set max value is > 100, then that max is used. If the set's min is <
	0, then that value is used.  Setting the forced values implies that the 
	natural range of the set is used.

	Set may be data_group.ACTIVE to select the active set.
*/
func ( dg *Data_group ) Get_nvalues( set int, forced_min float64, forced_max float64  ) ( []float64 ) {
	if dg == nil {
		return nil
	}

	if set < 0 || set > len( dg.sets ) {
		return dg.active.Get_nvalues( forced_min, forced_max )
	}

	return dg.sets[set].Get_nvalues( forced_min, forced_max )
}


/*
	Load_comms_sets loads  one or more sets with a common sequence value from file.
	The file is expected to be columns of values one of which may be the sequence value.
	Indicated columns from each newline separated record in the file is saved in a 
	separate data_set with a common sequence for all values in the record. The sequence
	value may be defined as a column in the input data, or may be a sequential integer
	incrased with each record read. 

	Parameters:
		fname - the name, fully or partially qualified, of the file to read
		sep - the character(s) used to separate fields (see note)
		seq - the column number containing the sequence value or -1 to generate an 
			  increasing value for each record.
		icols - either a string, or porinter to a string  containing space separated 
			  column numbers (e.g. "1 2 4") or an array where each element contails
			  a column number.

	Fields in the data may be separated by any character that is not a part of the data
	values (e.g. commas, semicolons, spaces, vertial bars). For all separators except
	spaces, empty fields are significant.  When spaces are used as separated, multiple
	contiguous spaces are considered one space. For example the following two records:
			1,,,2
			1   2

	would have 4 and 2 fields (columns) respecively. 
*/
func ( dg *Data_group ) Load_comms_sets( fname string, sep string, seq int, icols interface{} ) ( error )  {
	var (
		rerr error = nil				// must predeclare read loop error so for can see it
		cvalues []int
		n int
		sval float64
		tokens []string
		rec string						// cannot declare rec inside of for loop and still use err
	)

	if dg == nil {
		return ERR_NIL_GROUP
	}

	switch cols := icols.( type ) {						// split column list and convert to array of column numbers to snarf
		case string:
			cstrs := strings.Split( cols, " " )
			cvalues = make( []int, len( cstrs ) )
			for i, cs := range cstrs {
				cvalues[i] = clike.Atoi( cs )
			}	

		case *string:
			cstrs := strings.Split( *cols, " " )
			cvalues = make( []int, len( cstrs ) )
			for i, cs := range cstrs {
				cvalues[i] = clike.Atoi( cs )
			}	

		case []int:
			cvalues = cols
	}

	if len( cvalues ) > len( dg.sets ) {
		cvalues = cvalues[0:len( dg.sets )]
	}

	f, err := os.Open( fname ) 
	if err != nil {
		return err
	}

	breader := bufio.NewReader( f )
	need := 0							// set the minimum number of columns a record must have to be usable
	for _, v := range cvalues {
		if need < v + 1 {
			need = v + 1;
		}
	}
	if seq > need {
		need = seq
	}

	nr := 0;
	rerr = nil
	for ; rerr == nil ; {
		rec, rerr = breader.ReadString( '\n' )
		if rerr == nil {
			if sep == " " {
				n, tokens = token.Tokenise_populated( rec, sep )		// allows mutiple spaces 
			} else {
				n, tokens = token.Tokenise_qsep( rec, sep )				// quotes honoured, but more importantly treats ,, as 0 when sep is ,
			}

			if n >= need && tokens[0][0:1] != "#" {						// skip short/blank/commented lines
				if seq >= 0 {
					sval = clike.Atof( tokens[seq] )
				} else {
					sval = float64( nr )
				}
				nr += 1

				for i, cv := range cvalues {
					dg.sets[i].Add_sv( sval, clike.Atof( tokens[cv] ) )
				}
			}
		}
	}

	return nil
}

/*
	Add_sv adds a sequence/value pair to the active group as a 'push' onto the 
	data set.
*/
func( dg *Data_group ) Add_sv( seq float64, val float64 ) {
	if dg == nil || dg.active == nil {
		return
	}

	dg.active.Add_sv( seq, val )
}

/*
	Add_set_sv adds a sequence/value pair to the indicated set in the group as a 'push' onto the 
	data set.
*/
func( dg *Data_group ) Add_set_sv( set int, seq float64, val float64 ) {
	if dg == nil || dg.active == nil {
		return
	}

	if set < 0 || set > len( dg.sets ) {
		fmt.Printf( ">>> add-set-sv bad set number %d  len=%d\n", set, len( dg.sets ) )
		return
	}

	dg.sets[set].Add_sv( seq, val )
}

/*
	Merge allows a second datagroup to be added to the instance used to invoke this function.
	The contents of the src data group are added to the existing data.
*/
func ( dg *Data_group ) Add_group(  src *Data_group ) {
	if dg == nil || src == nil {
		return
	}

	new_sets := make( []*Data_set,len( dg.sets ) + src.Size() )
	copy( new_sets, dg.sets )
	copy( new_sets[len(dg.sets):], src.Get_sets() )

	dg.sets = new_sets
}

/*
	Get_max_value returns the largest value across all of the groups.
*/
func ( dg *Data_group ) Get_max_value(  ) ( float64 ) {
	if dg == nil {
		return 0
	}

	_, val := dg.sets[0].Get_value_range( )
	for i := 1; i < len( dg.sets ); i++ {
		_, nv := dg.sets[i].Get_value_range()
		if nv > val {
			val = nv
		}
	}

	return val
}

/*
	Get_min_value returns the smallest value across all of the groups.
*/
func ( dg *Data_group ) Get_min_value(  ) ( float64 ) {
	if dg == nil {
		return 0.0
	}

	val, _ := dg.sets[0].Get_value_range( )
	for i := 1; i < len( dg.sets ); i++ {
		nv, _ := dg.sets[i].Get_value_range( )
		if nv < val {
			val = nv
		}
	}

	return val
}


/*
	Get_value_range returns the min and max values across all datasets.
*/
func ( dg *Data_group ) Get_value_range(  ) ( min float64, max float64 ) {
	if dg == nil {
		return 0.0, 0.0
	}

	mn := dg.Get_min_value()
	mx := dg.Get_max_value()

	return mn, mx
}


/*
	Get_max_seq returns the largest sequence across all of the groups.
*/
func ( dg *Data_group ) Get_max_seq(  ) ( float64 ) {
	if dg == nil {
		return 0.0
	}

	_, val := dg.sets[0].Get_seq_range( )
	for i := 1; i < len( dg.sets ); i++ {
		_, nv := dg.sets[i].Get_seq_range()
		if nv > val {
			val = nv
		}
	}

	return val
}

/*
	Get_min_seq returns the smallest sequence across all of the groups.
*/
func ( dg *Data_group ) Get_min_seq(  ) ( float64 ) {
	if dg == nil {
		return 0.0
	}

	val, _ := dg.sets[0].Get_seq_range( )
	for i := 1; i < len( dg.sets ); i++ {
		nv, _ := dg.sets[i].Get_seq_range()
		if nv < val {
			val = nv
		}
	}

	return val
}


/*
	Get_seq_range returns the min and max sequences across all datasets.
*/
func ( dg *Data_group ) Get_seq_range(  ) ( min float64, max float64 ) {
	if dg == nil {
		return 0.0, 0.0
	}

	mn := dg.Get_min_seq()
	mx := dg.Get_max_seq()

	return mn, mx
}

/*
	Get_set_colour returns the colour for the indicated set.
*/
func ( dg *Data_group ) Get_set_colour( set int  )  ( string ) {
	if dg == nil {
		return "white"
	}

	return dg.sets[set].Get_colour()
}

/*
	Get_sets  returns the sets array.
*/
func ( dg *Data_group ) Get_sets(  )  []*Data_set {
	if dg == nil {
		return nil
	}

	return dg.sets
}

/*
	Get_set_size returns the size of each underlying data set.
*/
func ( dg *Data_group ) Get_set_size( ) ( int ) {
	if dg == nil {
		return 0
	}

	return dg.set_size
}


/*
	Size returns the number of data sets that exist in the group.
*/
func ( dg *Data_group ) Size( ) ( int ) {
	if dg == nil {
		return 0
	}

	return len( dg.sets )
}

/*
	String builds a string which represents the set.
*/
func ( dg *Data_group ) String( ) ( string ) {
	if dg == nil {
		return "<nil>"
	}

	s := ""

	for _, set := range dg.sets {
		s += fmt.Sprintf( "%s\n", set )
	}

	return s
}
